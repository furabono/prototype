<?xml version="1.0" encoding="UTF-8"?>
<tileset name="colony-db32-grounds-ready" tilewidth="16" tileheight="16" tilecount="1020" columns="20">
 <grid orientation="orthogonal" width="32" height="32"/>
 <image source="../tilesetres/16/colony-db32-extended/colony-db32-grounds-ready.png" width="320" height="816"/>
 <tile id="147">
  <objectgroup draworder="index">
   <object id="5" x="-6" y="6.27273" width="26.8182" height="13.4545"/>
  </objectgroup>
 </tile>
</tileset>
