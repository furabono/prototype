﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using GameCore.Class;
using GameCore.Utility;

using IronPython.Hosting;

public class PolicyManager : MonoBehaviour {

    [SerializeField]
    GameObject policyPanel;

    [SerializeField]
    GameObject policyScrollView;

    [SerializeField]
    GameObject policyScrollViewContent;

    [SerializeField]
    CivilManager civilManager;

    [SerializeField]
    GameObject policyButton;

    [SerializeField]
    Report report;

    Dictionary<PolicyPanel, Policy> policies;
    Dictionary<Policy, ReportData> reports;
    List<Tuple<Task, PolicyPanel>> tasks;
    bool initialized = false;

    void Start () {
        Init();
    }

    private void Update()
    {
        Tuple<Task, PolicyPanel> t;
        for (int i = 0; i < tasks.Count; ++i)
        {
            t = tasks[i];

            if (t.Item1.IsCompleted)
            {
                t.Item2.State = PolicyState.Enforced;
                tasks.RemoveAt(i);
                --i;
            }
        }
    }

    public void Init()
    {
        if (initialized) { return; }

        policies = new Dictionary<PolicyPanel, Policy>();
        reports = new Dictionary<Policy, ReportData>();
        tasks = new List<Tuple<Task, PolicyPanel>>();

        initialized = true;
    }

    public void AddPolicy(Policy policy)
    {
        var obj = Instantiate(policyPanel);
        var panel = obj.GetComponent<PolicyPanel>();
        panel.SetText(policy.Name);
        policies[panel] = policy;
        panel.AddClickListener(() => OnClick(panel));
        obj.transform.SetParent(policyScrollViewContent.transform);
        panel.transform.localScale = Vector3.one;
    }

    void OnClick(PolicyPanel panel)
    {
        if (panel.State == PolicyState.Enforceable)
        {
            var task = civilManager.Civil.ReadMindAsync(policies[panel]);
            panel.State = PolicyState.Enforcing;
            tasks.Add(Tuple.Create(task, panel));
        }
        else if (panel.State == PolicyState.Enforced)
        {
            ShowReport(policies[panel]);
        }
    }

    void ShowReport(Policy policy)
    {
        if (reports.ContainsKey(policy))
        {
            report.ShowReport(reports[policy]);
        }
        else
        {
            StartCoroutine(ShowReportCoroutine(policy));
        }
    }

    ReportData Research(Policy policy)
    {
        var dict = new Dictionary<float, int>();

        Civil civil = civilManager.Civil;
        float eval;
        int volume;
        for (int i = 0; i < civil.Count; ++i)
        {
            eval = civil[i][policy];
            if (!dict.TryGetValue(eval, out volume))
            {
                volume = 0;
            }
            dict[eval] = volume + 1;
        }

        var keys = new List<float>(dict.Keys);
        keys.Sort();

        ReportData rd;
        if (keys.Count <= 20)
        {
            var volumes = new int[keys.Count];
            for (int i = 0; i < keys.Count; ++i)
            {
                volumes[i] = dict[keys[i]];
            }

            rd = new ReportData(volumes, keys.ToArray());
        }
        else
        {
            float min = Mathf.Floor(keys[0]) - 1;
            float max = Mathf.Ceil(keys[keys.Count - 1]) + 1;
            float interval = (max - min) / 10.0f;
            var classes = new Tuple<float, float>[10];

            float low = min;
            float high = (int)(((min + interval) * 100) + 0.5f) / 100f;
            for (int i = 0; i < 10; ++i)
            {
                classes[i] = Tuple.Create(low, high);
                //low += interval;
                //high += interval;

                low = (int)(((low + interval) * 100) + 0.5f) / 100f;
                high = (int)(((high + interval) * 100) + 0.5f) / 100f;
            }

            var volumes = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int _class = 0;
            for (int i = 0; i < keys.Count; ++i)
            {
                if (keys[i] >= classes[_class].Item2) { ++_class; }
                volumes[_class] += dict[keys[i]];
            }

            rd = new ReportData(volumes, classes);
        }

        return rd;
    }

    IEnumerator ShowReportCoroutine(Policy policy)
    {
        var task = Task.Run(() => Research(policy));

        while (!task.IsCompleted) yield return null;

        ReportData result = task.Result;

        Debug.Log("1");
        reports[policy] = result;
        Debug.Log("2");
        report.ShowReport(result);
    }

    public IEnumerator LoadPolicyCoroutine()
    {
        var dir = new DirectoryInfo(Application.streamingAssetsPath + "/Policy");
        FileInfo[] fileInfos = dir.GetFiles("*.json");

        var task = Task.Run(() => LoadPolicy(fileInfos));

        while (!task.IsCompleted) yield return null;

        Policy[] policies = task.Result;

        for (int i = 0; i < policies.Length; ++i)
        {
            AddPolicy(policies[i]);
        }
    }

    Policy[] LoadPolicy(FileInfo[] fileInfos)
    {
        var policies = new Policy[fileInfos.Length];

        for (int i = 0; i < fileInfos.Length; ++i)
        {
            policies[i] = GameCore.Utility.DataManager.JsonToPolicy(fileInfos[i].FullName);
        }

        return policies;
    }

    public void OnPolicyButtonClick()
    {
        policyScrollView.SetActive(!policyScrollView.activeSelf);
    }
}
