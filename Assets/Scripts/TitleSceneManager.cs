﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleSceneManager : MonoBehaviour {
    private void Start()
    {
        DataManager.Instance.LoadAndInitAsync();
    }

    IEnumerator WaitForLoadingCoroutine()
    {
        while (DataManager.Instance.State == DataManagerState.Loading) { yield return null; }

        if (DataManager.Instance.State == DataManagerState.Loaded)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MovingTestScene");
        }
    }

    public void StartGame()
    {
        if (DataManager.Instance.State == DataManagerState.Loading)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
            StartCoroutine(WaitForLoadingCoroutine());
        }
        else if (DataManager.Instance.State == DataManagerState.Loaded)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MovingTestScene");
        }
    }

    public void Exit()
    {
        Application.Quit();
    }
}
