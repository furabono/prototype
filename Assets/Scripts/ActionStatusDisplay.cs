﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionStatusDisplay : MonoBehaviour {

    [SerializeField]
    new Camera camera;

    [SerializeField]
    Citizen target;

    [SerializeField]
    Text[] texts;

    Vector3 worldOffset;
    Vector3 screenOffset;
    Objective[] actions;

	void Start () {
        //texts = new Text[5];
        worldOffset = new Vector3(0.25f, 0f, 0.25f);
        screenOffset = new Vector3(40f, 40f, 0f);
        texts[3].text = "";
        texts[4].text = "";
	}
	
	void Update () {
		if (target)
        {
            transform.position = camera.WorldToScreenPoint(target.transform.position + worldOffset) + screenOffset;

            bool current = false;
            Objective temp;
            for (int i = 0; i < 3; ++i)
            {
                for (int j = i; j < actions.Length - 1; ++j)
                {
                    if (actions[j].Priority < actions[j + 1].Priority)
                    {
                        temp = actions[j];
                        actions[j] = actions[j + 1];
                        actions[j + 1] = temp;
                    }
                }
                if (ReferenceEquals(actions[i], target.CurrentObjective))
                {
                    current = true;
                    texts[i].color = Color.red;
                }
                else
                {
                    texts[i].color = Color.black;
                }
                texts[i].text = string.Format("{0}:{1}", actions[i].Name, actions[i].Priority);
            }

            if (!current)
            {
                //texts[4].text = string.Format("{0}:{1}", target.CurrentAction.Name, target.CurrentAction.Priority);
            }
        }
	}

    public void SetTarget(Citizen target)
    {
        this.target = target;
        actions = target.Actions;
    }

    public void OnClick()
    {
        target = null;
        gameObject.SetActive(false);
    }
}
