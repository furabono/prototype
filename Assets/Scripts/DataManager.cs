﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UnityEngine;
using BaseLib.Classes;

public class DataManager : MonoBehaviour {

    static DataManager _instance;
    public static DataManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);

            _state = DataManagerState.Unloaded;
            _streamingAssetsPath = Application.streamingAssetsPath;
        }
        else if (_instance != null)
        {
            Destroy(gameObject);
        }
    }

    string _streamingAssetsPath;

    DataManagerState _state;
    public DataManagerState State
    {
        get { return _state; }
    }

    Assembly _assembly;
    public Assembly Assembly
    {
        get { return _assembly; }
    }

    Type _citizenType;
    public Type CitizenType
    {
        get { return _citizenType; }
    }

    Type _worldType;
    public Type WorldType
    {
        get { return _worldType; }
    }

    ActionMetaBase[] _actionMetas;
    public ActionMetaBase[] ActionMetas
    {
        get { return _actionMetas; }
    }

    void Load()
    {
        _state = DataManagerState.Loading;
        {
            if (!File.Exists(_streamingAssetsPath + "/Export.mqd"))
            {
                _state = DataManagerState.Error;
                return;
            }

            _assembly = Assembly.LoadFile(_streamingAssetsPath + "/Export.mqd");
            _citizenType = _assembly.GetType("BaseLib.Classes.Citizen");
            _worldType = _assembly.GetType("BaseLib.Classes.World");
        }

        {
            var list = new List<ActionMetaBase>();
            var types = _assembly.GetTypes();
            ActionMetaBase am;

            for (int i = 0; i < types.Length; ++i)
            {
                if (types[i].BaseType.Name == "ActionMeta")
                {
                    am = GetInstance(types[i]) as ActionMetaBase;
                    list.Add(am);
                }
            }

            _actionMetas = list.ToArray();
        }
    }

    void Init()
    {
        for (int i = 0; i < _actionMetas.Length; ++i)
        {
            _actionMetas[i].Init(_streamingAssetsPath);
        }

        _state = DataManagerState.Loaded;
    }

    public void LoadAndInit()
    {
        Load();
        
        if (_state != DataManagerState.Error)
        {
            Init();
        }
    }

    public void LoadAndInitAsync()
    {
        StartCoroutine(LoadAndInitCoroutine());
    }

    IEnumerator LoadAndInitCoroutine()
    {
        {
            var task = Task.Run(new System.Action(Load));
            while (!task.IsCompleted) { yield return null; }
        }

        if (_state != DataManagerState.Error)
        {
            Init();
        }
    }

    public Func<object> GetActivator(Type type)
    {
        var newExp = NewExpression.New(type.GetConstructors()[0]);
        var lambda = Expression.Lambda<Func<object>>(newExp);
        return lambda.Compile();
    }

    public System.Action GetActionMetaInitializer(Type type)
    {
        var instanceExp = MemberExpression.Property(null, type, "Instance");
        var methodInfo = type.GetMethod("Init");
        var callExp = MethodCallExpression.Call(instanceExp, methodInfo);
        var lambda = Expression.Lambda<System.Action>(callExp);
        return lambda.Compile();
    }

    public object GetInstance(Type type)
    {
        var instanceExp = MemberExpression.Property(null, type, "Instance");
        var lambda = Expression.Lambda<Func<object>>(instanceExp);
        var compiled = lambda.Compile();
        return compiled();
    }

    public Texture2D LoadTexture(string file, FilterMode filterMode = FilterMode.Bilinear)
    {
        Texture2D tex = new Texture2D(0, 0);
        byte[] data = File.ReadAllBytes(file);

        ImageConversion.LoadImage(tex, data);
        tex.filterMode = filterMode;

        return tex;
    }

    public Sprite LoadSprite(string file)
    {
        Texture2D tex = LoadTexture(file);

        return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));
    }

    public Sprite LoadSprite(string file, Rect rect, Vector2 pivot)
    {
        Texture2D tex = LoadTexture(file);

        return Sprite.Create(tex, rect, pivot);
    }

    public Sprite LoadSprite(string file, Rect rect, Vector2 pivot, float pixelsPerUnit)
    {
        Texture2D tex = LoadTexture(file);

        return Sprite.Create(tex, rect, pivot, pixelsPerUnit);
    }
}

public enum DataManagerState { Unloaded, Loading, Loaded, Error };