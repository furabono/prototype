﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Newtonsoft.Json.Linq;

public class Tilemap : MonoBehaviour {

    [SerializeField] GameObject _tilePrefab;
    [SerializeField] GameObject _obstaclePrefab;

    NavMeshSurface _surface;
    Transform _layers;
    Transform _obstacles;

    int _width;
    public int Width { get { return _width; } }

    int _height;
    public int Height { get { return _height; } }

    int _citizenOrder;
    public int CitizenOrder { get { return _citizenOrder; } }

    private void Awake()
    {
        _surface = GetComponent<NavMeshSurface>();
        _layers = transform.Find("Layers");
        _obstacles = transform.Find("Obstacles");
    }

    public void Load(string name)
    {
        JObject obj;
        using (var stream = new System.IO.StreamReader(Application.streamingAssetsPath + "/maps/" + name + ".json"))
        using (var reader = new Newtonsoft.Json.JsonTextReader(stream))
        {
            obj = JObject.Load(reader);
        }

        _width = obj["width"].Value<int>();
        _height = obj["height"].Value<int>();

        transform.localScale = new Vector3(_width / 10, 1, _height / 10);

        int tileSize = obj["tilewidth"].Value<int>();

        var tilesets = new List<TileSet>();

        {
            JArray array = (JArray)obj["tilesets"];
            JToken tok;
            string[] split;
            string path;
            TileSet tileset;

            for (int i = 0; i < array.Count; ++i)
            {
                tok = array[i];
                split = tok["source"].Value<string>().Split(new string[] { @"\/" }, System.StringSplitOptions.None);
                path = Application.streamingAssetsPath + "/maps/tilesets/" + split[split.Length - 1];
                //for (int j = 0; j < split.Length - 1; ++j)
                //{
                //    path += split[j] + @"/";
                //}
                //path += split[split.Length - 1];

                tileset = ReadTsx(path);
                tileset.FirstGid = tok["firstgid"].Value<int>();
                tilesets.Add(tileset);
            }
        }

        {
            JArray layers = (JArray)obj["layers"];
            JToken layer;
            string layername;
            Transform tilesholder;
            JArray data;
            int value;
            GameObject tile;
            int col, row;
            for (int i = 0; i < layers.Count; ++i)
            {
                layer = layers[i];
                layername = layer["name"].Value<string>();

                if (layername == "Obstacle")
                {
                    data = (JArray)layer["data"];
                    for (int j = 0; j < data.Count; ++j)
                    {
                        if (data[j].Value<int>() == 0) { continue; }
                        tile = Instantiate(_obstaclePrefab);
                        tile.transform.parent = _obstacles;

                        col = j % _width;
                        row = j / _width;
                        tile.transform.position = new Vector3(0.5f + col - _width / 2, -0.5f - row + _height / 2, 0);
                    }
                    continue;
                }
                if (layername == "Citizen")
                {
                    _citizenOrder = i;
                    continue;
                }
                if (!layer["visible"].Value<bool>())
                {
                    continue;
                }

                tilesholder = new GameObject(layername).transform;
                tilesholder.parent = _layers;

                data = (JArray)layer["data"];
                for (int j = 0; j < data.Count; ++j)
                {
                    value = data[j].Value<int>();
                    if (value == 0) { continue; }

                    tile = CreateTile(value, tilesets, i);
                    tile.transform.parent = tilesholder;

                    col = j % _width;
                    row = j / _width;
                    tile.transform.position = new Vector3(0.5f + col - _width / 2, -0.5f - row + _height / 2, 0);
                }
            }
        }

        _surface.BuildNavMesh();
    }

    public TileSet ReadTsx(string path)
    {
        var xdoc = new System.Xml.XmlDocument();
        using (var stream = System.IO.File.OpenRead(path))
        {
            xdoc.Load(stream);
        }

        int tilesize;
        int columns;
        int tilecount;
        int height;
        Texture2D texture;

        var tileset = xdoc["tileset"];
        tilesize = int.Parse(tileset.GetAttribute("tilewidth"));
        columns = int.Parse(tileset.GetAttribute("columns"));

        tilecount = int.Parse(tileset.GetAttribute("tilecount"));

        var image = tileset["image"];
        height = int.Parse(image.GetAttribute("height"));

        string imagepath = image.GetAttribute("source");
        string[] split = imagepath.Split('/');
        imagepath = Application.streamingAssetsPath + "/maps/tilesets/" + split[split.Length - 1];
        texture = DataManager.Instance.LoadTexture(imagepath, FilterMode.Point);

        return new TileSet(texture, tilesize, columns, tilecount, height);
    }

    public GameObject CreateTile(int id, List<TileSet> tilesets, int order = 0)
    {
        var tile = Instantiate(_tilePrefab);
        var spriteRenderer = tile.GetComponent<SpriteRenderer>();

        for (int i = 0; i < tilesets.Count; ++i)
        {
            if (id >= tilesets[i].FirstGid && id <= tilesets[i].FirstGid + tilesets[i].TileCount)
            {
                spriteRenderer.sprite = tilesets[i].GetTileSprite(id - tilesets[i].FirstGid);
            }
        }

        spriteRenderer.sortingOrder = order;

        return tile;
    }
}

public class TileSet
{
    public int FirstGid { get; set; }

    public int TileCount { get; set; }

    public int Width { get; set; }

    public int Height { get; set; }

    public int TileSize { get; set; }

    public float PixelsPerUnit { get; set; }

    public Texture2D Texture { get; set; }

    int columns;
    int rows;
    Sprite[] spritecache;

    public TileSet(Texture2D texture, int tilesize, int columns, int tilecount, int height)
    {
        Texture = texture;
        this.columns = columns;
        TileSize = tilesize;
        TileCount = tilecount;
        Height = height;

        PixelsPerUnit = tilesize;

        spritecache = new Sprite[tilecount];
    }

    public Sprite GetTileSprite(int id)
    {
        if (spritecache[id] == null)
        {
            int col = id % columns;
            int row = id / columns;

            return spritecache[id] = Sprite.Create(Texture, new Rect(col * TileSize, Height - (row + 1) * TileSize, TileSize, TileSize), new Vector2(0.5f, 0.5f), PixelsPerUnit);
        }

        return spritecache[id];
    }
}