﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PolicyPanel : MonoBehaviour {
    
    [SerializeField]
    Sprite EnforceableSprite;

    [SerializeField]
    Sprite EnforcingSprite;

    [SerializeField]
    Sprite EnforcedSprite;

    PolicyState state;

    public PolicyState State
    {
        get { return state; }
        set
        {
            if (state != value)
            {
                Sprite sprite = null;

                switch (value)
                {
                    case PolicyState.Enforceable:
                        sprite = EnforceableSprite;
                        break;
                    case PolicyState.Enforcing:
                        sprite = EnforcingSprite;
                        break;
                    case PolicyState.Enforced:
                        sprite = EnforcedSprite;
                        break;
                }

                transform.Find("Button").Find("Image").GetComponent<Image>().sprite = sprite;
                state = value;
            }
        }
    }

    public void Start()
    {
        state = PolicyState.Enforceable;
    }

    public void AddClickListener(UnityAction listener)
    {
        transform.Find("Button").GetComponent<Button>().onClick.AddListener(listener);
    }

    public void SetText(string text)
    {
        transform.Find("Text").GetComponent<Text>().text = text;
    }
}

public enum PolicyState
{
    Enforceable, Enforcing, Enforced
}