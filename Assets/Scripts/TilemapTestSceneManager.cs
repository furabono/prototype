﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Newtonsoft.Json.Linq;

public class TilemapTestSceneManager : MonoBehaviour {

    [SerializeField] Tilemap _tilemap;
    [SerializeField] Camera _mainCamera;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        _tilemap.Load("test_map_4");

        _tilemap.transform.position = new Vector3(_tilemap.Width / 2 - 0.5f, _tilemap.Height / 2 - 0.5f, 0);
        _mainCamera.transform.position = new Vector3(_tilemap.Width / 2 - 0.5f, _tilemap.Height / 2 - 0.5f, _mainCamera.transform.position.z);
    }
}