﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using GameCore.Class;
using GameCore.Utility;

public class CivilManager : MonoBehaviour {

    [SerializeField]
    GameObject selectButton;

    [SerializeField]
    GameObject buttonSelector;

    [SerializeField]
    GameObject civilScrollViewContent;

    Dictionary<SelectOne, FileInfo> fileInfos;
    SelectOneGroup group;
    Civil civil;
    bool initialized = false;

    public Civil Civil
    {
        get { return civil; }
    }

	void Start () {
        Init();
	}

    public void Init()
    {
        if (initialized) { return; }

        fileInfos = new Dictionary<SelectOne, FileInfo>();

        var dir = new DirectoryInfo(Application.streamingAssetsPath + "/Civil");
        FileInfo[] files = dir.GetFiles("*.json");

        group = buttonSelector.GetComponent<SelectOneGroup>();
        var content = civilScrollViewContent.transform;
        GameObject obj;
        string name;
        SelectOne so;

        for (int i = 0; i < files.Length; ++i)
        {
            name = files[i].Name;
            name = name.Substring(0, name.Length - 5);

            obj = Instantiate(selectButton);
            obj.transform.Find("Text").GetComponent<Text>().text = name;

            so = obj.GetComponent<SelectOne>();
            group.Register(so);
            fileInfos[so] = files[i];

            obj.transform.SetParent(content);
            obj.transform.localScale = Vector3.one;
        }

        initialized = true;

    }

    public FileInfo GetSelectedFile()
    {
        if (group.Selected == null) return null;
        else return fileInfos[group.Selected];
    }

    public void LoadCivil(FileInfo fileInfo)
    {
        civil = GameCore.Utility.DataManager.JsonToCivil(fileInfo.FullName);
    }
}
