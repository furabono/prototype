﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using GameCore;

public class Citizen : MonoBehaviour {

    PathManager pathManager;

    [SerializeField]
    int numArea;

    [SerializeField]
    float speed;

    HiddenMarkovMachine hiddenMarkovMachine;
    InputState input1;
    InputState input2;
    InputState input3;
    public int output;
    List<Objective> objectives;
    Stack<Objective> objectiveStack;
    Objective currentObjective;

    NavMeshAgent agent;
    Area area;

    [SerializeField]
    Dictionary<string, float> parameters;

    public class Friend
    {
        protected static NavMeshAgent Agent(Citizen citizen) { return citizen.agent; }
        protected static Area Area(Citizen citizen) { return citizen.area; }
        protected static void Area(Citizen citizen, Area area) { citizen.area = area; }
        protected static Objective TopObjective(Citizen citizen) { return citizen.currentObjective; }
        protected static Objective SecondObjective(Citizen citizen) { return citizen.objectiveStack.Peek(); }
        protected static void PushObjective(Citizen citizen, Objective objective)
        {
            citizen.objectiveStack.Push(citizen.currentObjective);
            citizen.currentObjective = objective;
        }
        protected static float Psychology(Citizen citizen, string name) { return citizen.parameters[name]; }
        protected static void Psychology(Citizen citizen, string name, float value) { citizen.parameters[name] = value; }
        protected static float Ability(Citizen citizen, string name) { return citizen.parameters[name]; }
        protected static void Ability(Citizen citizen, string name, float value) { citizen.parameters[name] = value; }
        protected static float State(Citizen citizen, string name) { return citizen.parameters[name]; }
        protected static void State(Citizen citizen, string name, float value) { citizen.parameters[name] = value; }
    }

    public Objective[] Actions
    {
        get { return objectives.ToArray(); }
    }

    public Objective CurrentObjective
    {
        get { return currentObjective; }
    }

	void Start () {
        pathManager = PathManager.Instance;
        agent = GetComponent<NavMeshAgent>();

        //InitHMM();
        //input2.Value = Random.Range(-100f, 100f);
        //input3.Value = Random.Range(-100f, 100f);

        area = pathManager.GetArea(Random.Range(0, numArea));
        agent.Warp(area.GetRandomCoord());
        //transform.position = area.GetRandomCoord();

        parameters = new Dictionary<string, float>
        {
            {"Satiety", Random.Range(30, 60) },
            {"Food", Random.Range(40, 100) },
            {"Strength", Random.Range(0, 100) },
            {"Concentration", Random.Range(0, 100) },
            {"Endurance", Random.Range(0, 100) }
        };

        objectives = new List<Objective>
        {
            new OEat(this),
            new OSleep(this),
            new OGetFood(this)
        };
        objectiveStack = new Stack<Objective>();
        currentObjective = null;
	}

    void InitHMM()
    {
        input1 = new InputState();
        input2 = new InputState();
        input3 = new InputState();

        State[] inputs = new State[3] { input1, input2, input3 };

        State[] hidden1 = new State[7];
        {
            State[] valueParents = new State[0];
            float[] weights = new float[0];
            State[] workParents = new State[1] { input1 };

            for (int i = 0; i < 6; ++i)
            {
                hidden1[i] = new HiddenState(
                    valueParents, 
                    weights, 
                    0f,
                    workParents, 
                    new float[1] { i - 0.50f }, 
                    new float[1] { i + 0.49f });
            }

            hidden1[6] = new HiddenState(
                valueParents,
                weights,
                0f,
                workParents,
                new float[1] { -2.2f },
                new float[1] { -0.51f });
        }

        State[] hidden2 = new State[3];
        {
            State[] valueParents = new State[2] { input2, input3 };
            State[] workParents = new State[0];
            
            for (int i = 0; i < 3; ++i)
            {
                hidden2[i] = new HiddenState(
                    valueParents, 
                    new float[2] { Random.Range(-10f, 10f), Random.Range(-10f, 10f) }, 
                    Random.Range(-10f, 10f), 
                    workParents, 
                    null, 
                    null);
            }
        }

        State[] hidden3 = new State[3];
        {
            State[] workParents = new State[0];

            for (int i = 0; i < 3; ++i)
            {
                hidden3[i] = new HiddenState(
                    hidden2, 
                    new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) },
                    Random.Range(-10f, 10f),
                    workParents, 
                    null, 
                    null);
            }
        }

        /*State[] outputs = new State[7];
        {        
            for (int i = 0; i < 6; ++i)
            {
                outputs[i] = new HiddenState(hidden3
                    , new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) }
                    , Random.Range(-10f, 10f)
                    , new State[1] { hidden1[i] }
                    , new float[1] { 1 }
                    , new float[1] { -1 });
            }

            outputs[6] = new HiddenState(hidden3
                , new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) }
                , 100000f
                , new State[1] { hidden1[6] }
                , new float[1] { 1 }
                , new float[1] { -1 });
        }*/
        State[] outputs = new State[6];
        {
            State[] workParents = new State[0];
            for (int i = 0; i < 6; ++i)
            {
                outputs[i] = new HiddenState(hidden3
                    , new float[3] { Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f) }
                    , Random.Range(-10f, 10f)
                    , workParents
                    , null
                    , null);
            }
        }

        State[][] layers = new State[4][] { inputs, hidden2, hidden3, outputs };
        hiddenMarkovMachine = new HiddenMarkovMachine(layers);
    }

    void Update()
    {
        //input1.Value = (area != null) ? ((float)area.Index) : (-1f);
        //input2.Value = input2.Value + Random.Range(-1f, 1f);
        //input3.Value = input2.Value + Random.Range(-1f, 1f);
        //hiddenMarkovMachine.Tick();

        {
            for (int i = 0; i < objectives.Count; ++i)
            {
                if (objectives[i].Priority == 0)
                {
                    if (currentObjective != null)
                    {
                        if (objectives[i].Name == currentObjective.Name || objectiveStack.Contains(objectives[i]))
                        {
                            continue;
                        }
                        objectiveStack.Push(currentObjective);
                    }
                    currentObjective = objectives[i];
                    break;
                }
            }
        }

        if (currentObjective != null)
        {
            currentObjective.Do();
        }

        if (currentObjective == null || !currentObjective.BeingPerformed)
        {
            if (objectiveStack.Count > 0)
            {
                currentObjective = objectiveStack.Pop();
            }
            else
            {
                currentObjective = objectives[Random.Range(0, objectives.Count - 1)];
                for (int i = 0; i < objectives.Count; ++i)
                {
                    if (objectives[i].Priority > currentObjective.Priority)
                    {
                        currentObjective = objectives[i];
                    }
                }
            }
        }

        --parameters["Satiety"];

        if (parameters["Satiety"] < 0)
        {
            gameObject.SetActive(false);
        }
    }
}

public class HiddenMarkovMachine
{
    State[][] layers;
    State[] output;

    public State[] Output
    {
        get { return output; }
    }

    public HiddenMarkovMachine(State[][] layers)
    {
        this.layers = layers;
        output = layers[layers.Length - 1];
    }

    public void Tick()
    {
        State[] layer = null;
        for (int i = 0; i < layers.Length;  ++i)
        {
            layer = layers[i];
            for (int j = 0; j < layer.Length; ++j)
            {
                layer[j].Update();
            }
        }
    }
}

public interface State
{
    float Value
    {
        get;
    }

    bool Work
    {
        get;
    }

    void Update();
}

public class InputState : State
{
    float value;
    bool work;

    public float Value
    {
        get { return value; }
        set { this.value = value; }
    }

    public bool Work
    {
        get { return work; }
        set { work = value; }
    }

    public InputState()
    {
        //value = Random.Range(0f, 100f);
        value = 0;
        work = true;
    }

    public void Update()
    {
        //value += Random.Range(-5f, 5f);
        //if (value < 0f) { value = 0f; }
        //else if (value > 100f) { value = 100f; }
    }
}

public class HiddenState : State
{
    float value;
    bool work;
    State[] valueParents;
    float[] weights;
    float bias;
    State[] workParents;
    float[] mins;
    float[] maxs;

    public float Value
    {
        get { return value; }
    }

    public bool Work
    {
        get { return work; }
    }

    public HiddenState(State[] valueParents, float[] weights, float bias, State[] workParents, float[] mins, float[] maxs)
    {
        this.valueParents = valueParents;
        this.weights = weights;
        this.workParents = workParents;
        this.mins = mins;
        this.maxs = maxs;

        this.value = 0f;
        this.work = true;
    }

    public void Update()
    {
        //value = null;
        //work = null;

        work = true;
        for (int i = 0; i < workParents.Length; ++i)
        {
            if (workParents[i].Work && (workParents[i].Value < mins[i] || workParents[i].Value > maxs[i]))
            {
                work = false;
                break;
            }
        }

        if (!work) { return; }

        value = bias;
        for (int i = 0; i < valueParents.Length; ++i)
        {
            value += (valueParents[i].Work) ? (valueParents[i].Value * weights[i]) : (0f);
        }
    }
}

public abstract class Objective : Citizen.Friend
{
    protected Citizen target;
    protected int priority;
    protected int priorityUpdatedTick;
    protected bool beingPerformed;
    protected System.Action do_;

    public int Priority
    {
        get
        {
            if (World.Tick > priorityUpdatedTick)
            {
                UpdatePriority();
            }
            return priority;
        }
    }
    public bool BeingPerformed
    {
        get { return beingPerformed; }
    }

    public abstract string Name
    {
        get;
    }

    public Objective(Citizen target)
    {
        this.target = target;
        this.priority = 5;
        this.priorityUpdatedTick = 0;
        this.beingPerformed = false;
    }

    public void Do()
    {
        do_();
    }
    protected virtual void Start()
    {
        beingPerformed = true;
    }
    protected abstract void Update();
    public virtual void Terminate()
    {
        beingPerformed = false;
    }
    protected abstract void UpdatePriority();
}

public class OEat :Objective
{
    OGetFood oGetFood;
    AEat aEat;

    public override string Name
    {
        get { return "EAT"; }
    }

    public OEat(Citizen target) : base(target)
    {
        oGetFood = new OGetFood(target);
        aEat = new AEat(target);
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();
        do_ = Update;
        if (State(target, "Food") < 50 - Psychology(target, "Satiety"))
        {
            PushObjective(target, oGetFood);
        }
    }

    protected override void Update()
    {
        aEat.Do();

        //beingPerformed = satiety >= 50;
        if (Psychology(target, "Satiety") >= 50)
        {
            Terminate();
        }
    }

    public override void Terminate()
    {
        base.Terminate();

        if (aEat.BeingPerformed)
        {
            aEat.Terminate();
        }
        do_ = Start;
    }

    protected override void UpdatePriority()
    {
        float satiety = Psychology(target, "Satiety");

        if (satiety < 10)
        {
            priority = 0;
        }
        else if (satiety < 20)
        {
            priority = 9;
        }
        else if (satiety < 30)
        {
            priority = 7;
        }
        else
        {
            priority = 5;
        }
    }
}

public class OSleep :Objective
{
    ASleep aSleep;

    public override string Name
    {
        get { return "SLEEP"; }
    }

    public OSleep(Citizen target) : base(target)
    {
        aSleep = new ASleep(target);
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();

        do_ = Update;
    }

    protected override void Update()
    {
        aSleep.Do();

        if ((World.Time > 480) || (Psychology(target, "Endurance") >= 90 && (Ability(target, "Strength") >= 90 || Psychology(target, "Concentration") >= 90)))
        {
            Terminate();
        }
    }

    public override void Terminate()
    {
        base.Terminate();

        if (aSleep.BeingPerformed)
        {
            aSleep.Terminate();
        }

        do_ = Start;
    }

    protected override void UpdatePriority()
    {
        int count = 0;
        float strength = Ability(target, "Strength");
        float concentration = Psychology(target, "Concentration");
        float endurance = Psychology(target, "Endurance");

        if (strength < 10 || concentration < 10)
        {
            count += 2;
        }
        else if (strength < 30 || concentration < 10)
        {
            count += 1;
        }

        if (endurance < 10)
        {
            count += 2;
        }
        else if (endurance < 30)
        {
            count += 1;
        }

        if (count >= 2)
        {
            priority = 0;
        }
        else if (World.Time > 0 && World.Time < 480)
        {
            priority = 8;
        }
    }
}

public class OGetFood :Objective
{
    Action[] actions;
    Action action;

    public override string Name
    {
        get { return "GET_FOOD"; }
    }

    public OGetFood(Citizen target) : base(target)
    {
        actions = new Action[] { new AHunt(target), new AGather(target) };
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();

        action = actions[0];

        for (int i = 1; i < actions.Length; ++i)
        {
            if (actions[i].Efficiency > action.Efficiency)
            {
                action = actions[i];
            }
        }

        do_ = Update;
    }

    protected override void Update()
    {
        action.Do();

        if (State(target, "Food") >= 50 || Psychology(target, "Satiety") < 5)
        {
            Terminate();
        }
    }

    public override void Terminate()
    {
        base.Terminate();

        if (action.BeingPerformed)
        {
            action.Terminate();
        }

        do_ = Start;
    }

    protected override void UpdatePriority()
    {
        float food = State(target, "Food");

        if (food < 10)
        {
            priority = 9;
        }
        else if (food < 30)
        {
            priority = 7;
        }
        else if (food < 50)
        {
            priority = 6;
        }
    }
}

public abstract class Action :Citizen.Friend
{
    protected Citizen target;
    protected float efficiency;
    protected int efficiencyUpdatedTick;
    protected bool beingPerformed;
    protected System.Action do_;

    public abstract string Name
    {
        get;
    }

    public float Efficiency
    {
        get
        {
            if (World.Tick > efficiencyUpdatedTick)
            {
                UpdateEfficiency();
            }
            return efficiency;
        }
    }

    public bool BeingPerformed
    {
        get { return beingPerformed; }
    }

    public Action(Citizen target)
    {
        this.target = target;
        beingPerformed = true;
        efficiency = 0;
        efficiencyUpdatedTick = 0;
    }

    public void Do()
    {
        do_();
    }
    protected virtual void Start()
    {
        beingPerformed = false;
    }
    protected abstract void Update();
    public virtual void Terminate()
    {
        beingPerformed = false;
    }
    protected abstract void UpdateEfficiency();
}

public class AHunt : Action
{
    AMoveArea moveArea;
    AMoveRandomPosInArea movePos;
    System.Action update;

    public override string Name
    {
        get { return "HUNT"; }
    }

    public AHunt(Citizen target) : base(target)
    {
        moveArea = new AMoveArea(target, PathManager.Instance.GetArea(1));
        movePos = new AMoveRandomPosInArea(target);
        do_ = Start;
    }

    void MoveArea()
    {
        moveArea.Do();

        if (!moveArea.BeingPerformed)
        {
            update = Hunt;
        }
    }

    void Hunt()
    {
        movePos.Do();

        float food = State(target, "Food") + 2;
        State(target, "Food", food);

        float strength = Ability(target, "Strength") - 2;
        Ability(target, "Strength", strength);

        float concentration = Psychology(target, "Concentration") - 1;
        Psychology(target, "Concentration", concentration);

        float endurance = Psychology(target, "Endurance") - 1;
        Psychology(target, "Endurance", endurance);

        float satiety = Psychology(target, "Satiety") - 2;
        Psychology(target, "Satiety", satiety);

        //beingPerformed = food >= 60;
        //if (food >= 60)
        //{
        //    Terminate();
        //}
    }

    protected override void Start()
    {
        // 임시 : Room2로 이동
        update = MoveArea;
        do_ = Update;
    }

    protected override void Update()
    {
        update();
    }

    public override void Terminate()
    {
        base.Terminate();

        if (moveArea.BeingPerformed)
        {
            moveArea.Terminate();
        }
        if (movePos.BeingPerformed)
        {
            movePos.Terminate();
        }
        update = null;
        do_ = Start;
    }

    protected override void UpdateEfficiency()
    {
        // ToDo...
    }
}

public class AGather : Action
{
    AMoveArea moveArea;
    AMoveRandomPosInArea movePos;
    System.Action update;

    public override string Name
    {
        get { return "GATHER"; }
    }

    public AGather(Citizen target) : base(target)
    {
        moveArea = new AMoveArea(target, PathManager.Instance.GetArea(2));
        movePos = new AMoveRandomPosInArea(target);
        do_ = Start;
    }

    void MoveArea()
    {
        moveArea.Do();

        if (!moveArea.BeingPerformed)
        {
            update = Gather;
        }
    }

    void Gather()
    {
        movePos.Do();

        float food = State(target, "Food") + 1;
        State(target, "Food", food);

        float strength = Ability(target, "Strength") - 1;
        Ability(target, "Strength", strength);

        float concentration = Psychology(target, "Concentration") - 1;
        Psychology(target, "Concentration", concentration);

        float endurance = Psychology(target, "Endurance") - 2;
        Psychology(target, "Endurance", endurance);

        float satiety = Psychology(target, "Satiety") - 1;
        Psychology(target, "Satiety", satiety);

        //beingPerformed = food >= 60;
        //if (food >= 50)
        //{
        //    Terminate();
        //}
    }

    protected override void Start()
    {
        // 임시 : Room3로 이동
        update = MoveArea;
        do_ = Update;
    }

    protected override void Update()
    {
        update();
    }

    public override void Terminate()
    {
        base.Terminate();

        if (moveArea.BeingPerformed)
        {
            moveArea.Terminate();
        }
        if (movePos.BeingPerformed)
        {
            movePos.Terminate();
        }
        update = null;
        do_ = Start;
    }

    protected override void UpdateEfficiency()
    {
        // ToDo...
    }
}

public class AEat :Action
{
    public override string Name
    {
        get { return "EAT"; }
    }

    public AEat(Citizen target) : base(target)
    {
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();
        do_ = Update;
    }

    protected override void Update()
    {
        float satiety = Psychology(target, "Satiety") + 5;
        Psychology(target, "Satiety", satiety);
        State(target, "Food", State(target, "Food") - 1);

        //beingPerformed = satiety >= 50;
        //if (satiety >= 50)
        //{
        //    Terminate();
        //}
    }

    public override void Terminate()
    {
        base.Terminate();

        do_ = Start;
    }

    protected override void UpdateEfficiency()
    {
        // Do Nothing
    }
}

public class ASleep : Action
{
    AMoveArea moveArea;
    AMoveRandomPosInArea movePos;
    System.Action update;

    public override string Name
    {
        get { return "SLEEP"; }
    }

    public ASleep(Citizen target) : base(target)
    {
        moveArea = new AMoveArea(target, PathManager.Instance.GetArea(0));
        movePos = new AMoveRandomPosInArea(target);
        do_ = Start;
    }

    void MoveArea()
    {
        moveArea.Do();

        if (!moveArea.BeingPerformed)
        {
            update = MovePos;
        }
        else if (TopObjective(target).Priority == 0)
        {
            moveArea.Terminate();
            update = Sleep;
        }
    }

    void MovePos()
    {
        movePos.Do();

        if (!movePos.BeingPerformed)
        {
            update = Sleep;
        }
        else if (TopObjective(target).Priority == 0)
        {
            movePos.Terminate();
            update = Sleep;
        }
    }

    void Sleep()
    {
        float strength = Ability(target, "Strength") + 1;
        Ability(target, "Strength", strength);

        float concentration = Psychology(target, "Concentration") + 1;
        Psychology(target, "Concentration", concentration);

        float endurance = Psychology(target, "Endurance") + 1;
        Psychology(target, "Endurance", endurance);

        float satiety = Psychology(target, "Satiety") - 2;
        Psychology(target, "Satiety", satiety);

        //beingPerformed = (World.Time > 480) || (endurance >= 90 && (strength >= 90 || concentration >= 90));
        //if ((World.Time > 480) || (endurance >= 90 && (strength >= 90 || concentration >= 90)))
        //{
        //    Terminate();
        //}
    }

    protected override void Start()
    {
        base.Start();
        do_ = Update;

        // 거처 있는경우 거처로 이동
        // 임시 : Room1으로 이동
        if (TopObjective(target).Priority > 0)
        {
            update = MoveArea;
        }
        else
        {
            update = Sleep;
        }
    }

    protected override void Update()
    {
        update();
    }

    public override void Terminate()
    {
        base.Terminate();

        if (moveArea.BeingPerformed)
        {
            moveArea.Terminate();
        }
        if (movePos.BeingPerformed)
        {
            movePos.Terminate();
        }
        update = null;
        do_ = Start;
    }

    protected override void UpdateEfficiency()
    {
        // Do Nothing
    }
}

public class AMoveArea :Action
{
    Area area;
    Path path;
    System.Action update;
    NavMeshAgent agent;

    public override string Name
    {
        get { return "MoveArea"; }
    }

    public AMoveArea(Citizen target)
        :base(target)
    {
        agent = Agent(target);
        path = null;
    }

    public AMoveArea(Citizen target, Area area)
        :this(target)
    {
        this.area = area;
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();

        do_ = Update;

        if (object.ReferenceEquals(area, Area(target)))
        {
            Terminate();
        }
        else if (Area(target) == null)
        {
            update = MoveToArea0;
        }
        else
        {
            update = TracePath0;
        }
    }

    protected override void Update()
    {
        update();
    }

    public override void Terminate()
    {
        base.Terminate();

        do_ = Start;

        if (agent.pathPending || agent.remainingDistance > float.Epsilon)
        {
            agent.isStopped = true;
        }
        update = null;
        path = null;
    }

    void SetArea(Area area)
    {
        this.area = area;
    }

    void MoveToArea0()
    {
        Vector3 randomGate = area.Gates[Random.Range(0, area.Gates.Length)];
        agent.destination = randomGate;
        update = MoveToArea1;
    }

    void MoveToArea1()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Terminate();
        }
    }

    void TracePath0()
    {
        path = PathManager.Instance.GetPath(Area(target).Index, area.Index);
        agent.destination = path.NavMeshPath.corners[0];
        update = TracePath1;
    }

    void TracePath1()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Area(target, null);
            agent.SetPath(path.NavMeshPath);
            update = TracePath2;
        }
    }

    void TracePath2()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Area(target, area);
            Terminate();
        }
    }

    protected override void UpdateEfficiency()
    {
        // Do Nothing...
    }
}

public class AMoveRandomPosInArea : Action
{
    Area area;
    NavMeshAgent agent;

    public override string Name
    {
        get { return "MoveRandomPosInArea"; }
    }

    public AMoveRandomPosInArea(Citizen target)
        : base(target)
    {
        agent = Agent(target);
        do_ = Start;
    }

    protected override void Start()
    {
        base.Start();

        area = Area(target);

        do_ = Update;
        
        if (area == null)
        {
            Terminate();
        }
        else
        {
            agent.destination = area.GetRandomCoord();
        }
    }

    protected override void Update()
    {
        if (!agent.pathPending && agent.remainingDistance <= float.Epsilon)
        {
            Terminate();
        }
    }

    public override void Terminate()
    {
        base.Terminate();

        do_ = Start;

        if (agent.pathPending || agent.remainingDistance > float.Epsilon)
        {
            agent.isStopped = true;
        }
    }

    protected override void UpdateEfficiency()
    {
        // Do Nothing...
    }
}

public interface IEstimator
{
    float Estimation
    {
        get;
    }
}

public class EConstant :IEstimator
{
    static Dictionary<float, EConstant> instances;
    float value;

    public float Estimation
    {
        get { return value; }
    }

    private EConstant(float value)
    {
        this.value = value;
        instances.Add(value, this);
    }

    public static EConstant Instance(float value)
    {
        if (instances == null)
        {
            instances = new Dictionary<float, EConstant>();
        }

        EConstant instance;
        instances.TryGetValue(value, out instance);

        if (instance == null)
        {
            instance = new EConstant(value);
            instances.Add(value, instance);
        }

        return instance;
    }
}

public class EPsychology :Citizen.Friend, IEstimator
{
    static Dictionary<string, EPsychology> instances;
    string parameterName;
    Citizen target;

    public float Estimation
    {
        get { return Psychology(target, parameterName); }
    }

    private EPsychology(string parameterName)
    {
        this.parameterName = parameterName;
        instances.Add(parameterName, this);
    }

    public EPsychology Instance(string parameterName)
    {
        if (instances == null)
        {
            instances = new Dictionary<string, EPsychology>();
        }

        EPsychology instance;
        instances.TryGetValue(parameterName, out instance);

        if (instance == null)
        {
            instance = new EPsychology(parameterName);
            instances.Add(parameterName, instance);
        }

        return instance;
    }

    public void SetTarget(Citizen target)
    {
        // target != null
        this.target = target;
    }
}

public class EAbility : Citizen.Friend, IEstimator
{
    static Dictionary<string, EAbility> instances;
    string parameterName;
    Citizen target;

    public float Estimation
    {
        get { return Ability(target, parameterName); }
    }

    private EAbility(string parameterName)
    {
        this.parameterName = parameterName;
        instances.Add(parameterName, this);
    }

    public EAbility Instance(string parameterName)
    {
        if (instances == null)
        {
            instances = new Dictionary<string, EAbility>();
        }

        EAbility instance;
        instances.TryGetValue(parameterName, out instance);

        if (instance == null)
        {
            instance = new EAbility(parameterName);
            instances.Add(parameterName, instance);
        }

        return instance;
    }

    public void SetTarget(Citizen target)
    {
        // target != null
        this.target = target;
    }
}

public class EState : Citizen.Friend, IEstimator
{
    static Dictionary<string, EState> instances;
    string parameterName;
    Citizen target;

    public float Estimation
    {
        get { return State(target, parameterName); }
    }

    private EState(string parameterName)
    {
        this.parameterName = parameterName;
        instances.Add(parameterName, this);
    }

    public EState Instance(string parameterName)
    {
        if (instances == null)
        {
            instances = new Dictionary<string, EState>();
        }

        EState instance;
        instances.TryGetValue(parameterName, out instance);

        if (instance == null)
        {
            instance = new EState(parameterName);
            instances.Add(parameterName, instance);
        }

        return instance;
    }

    public void SetTarget(Citizen target)
    {
        // target != null
        this.target = target;
    }
}

public class EAdd
{

}

namespace TTest
{
    public class RPNCalculator
    {
        Stack<float> stack;

        public class Expression
        {
            
        }

        public enum Operator
        {
            NOT,
            AND,
            OR,
            EQUAL,
            BIGGER,
            SMALLER,
            BIGGER_OR_EQUAL,
            SMALLER_OR_EQUAL,
            ADD,
            SUBTRACT,
            MULTIPLY,
            DIVIDE,
            MOD,
            ABS,
            POWER,
            SQRT,
            LOG,
            LOG_2,
            LOG_10,
            LOG_E,
            FACTORIAL
        }

        void Operate(Operator op)
        {
            switch (op)
            {
                case Operator.NOT:
                    {
                        Not();
                        break;
                    }
                case Operator.AND:
                    {
                        And();
                        break;
                    }
                case Operator.OR:
                    {
                        Or();
                        break;
                    }
                case Operator.EQUAL:
                    {
                        Equal();
                        break;
                    }
                case Operator.BIGGER:
                    {
                        Bigger();
                        break;
                    }
                case Operator.SMALLER:
                    {
                        Smaller();
                        break;
                    }
                case Operator.BIGGER_OR_EQUAL:
                    {
                        BiggerOrEqual();
                        break;
                    }
                case Operator.SMALLER_OR_EQUAL:
                    {
                        SmallerOrEqual();
                        break;
                    }
                case Operator.ADD:
                    {
                        Add();
                        break;
                    }
                case Operator.SUBTRACT:
                    {
                        Subtract();
                        break;
                    }
                case Operator.MULTIPLY:
                    {
                        Multiply();
                        break;
                    }
                case Operator.DIVIDE:
                    {
                        Divide();
                        break;
                    }
            }
        }

        bool FloatToBool(float value)
        {
            return value > 0f;
        }

        float BoolToFloat(bool value)
        {
            return (value ? 1f : 0f);
        }

        void Not()
        {
            stack.Push(BoolToFloat(!FloatToBool(stack.Pop())));
        }
        void And()
        {
            stack.Push(BoolToFloat(FloatToBool(stack.Pop()) && FloatToBool(stack.Pop())));
        }
        void Or()
        {
            stack.Push(BoolToFloat(FloatToBool(stack.Pop()) || FloatToBool(stack.Pop())));
        }
        void Equal()
        {
            stack.Push(BoolToFloat(stack.Pop() == stack.Pop()));
        }
        void Bigger()
        {
            stack.Push(BoolToFloat(stack.Pop() > stack.Pop()));
        }
        void Smaller()
        {
            stack.Push(BoolToFloat(stack.Pop() < stack.Pop()));
        }
        void BiggerOrEqual()
        {
            stack.Push(BoolToFloat(stack.Pop() >= stack.Pop()));
        }
        void SmallerOrEqual()
        {
            stack.Push(BoolToFloat(stack.Pop() <= stack.Pop()));
        }
        void Add()
        {
            stack.Push(stack.Pop() + stack.Pop());
        }
        void Subtract()
        {
            stack.Push(-stack.Pop() + stack.Pop());
        }
        void Multiply()
        {
            stack.Push(stack.Pop() * stack.Pop());
        }
        void Divide()
        {
            float right = stack.Pop();
            stack.Push(stack.Pop() / right);
        }
        void Mod()
        {
            float right = stack.Pop();
            stack.Push(stack.Pop() % right);
        }
        void Abs()
        {
            stack.Push(Mathf.Abs(stack.Pop()));
        }
    }
}

namespace Utility
{
    public abstract class SingleTon<T> where T:new()
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }

                return instance;
            }
        }
    }
}