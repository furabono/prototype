﻿//using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using GameCore;

public class PathManager : MonoBehaviour {

    [SerializeField]
    GameObject world;

    [SerializeField]
    Tiled2Unity.TiledMap tilemap;

    static PathManager instance;
    Area[] idx2area;
    Dictionary<string, Area> name2area;
    Path[,] paths;

    public static PathManager Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Init();
        }
        else if(instance != this)
        {
            Destroy(this);
        }
    }

    public GameObject line;
    void Init()
    {
        Collider2D[] children = tilemap.transform.GetComponentsInChildren<Collider2D>();

        // Initialize Areas
        {
            Collider2D[] _areas = (from c in children
                                   where c.gameObject.CompareTag("Area")
                                   select c).ToArray();

            List<Vector3>[] coords = new List<Vector3>[_areas.Length];
            for (int i = 0; i < coords.Length; ++i)
            {
                coords[i] = new List<Vector3>();
            }

            int height = tilemap.NumTilesHigh;
            int width = tilemap.NumTilesWide;
            float x, y;
            for (int i = 0; i < height; ++i)
            {
                y = i + 0.5f - height;
                for (int j = 0; j < width; ++j)
                {
                    x = j + 0.5f;
                    for (int k = 0; k < _areas.Length; ++k)
                    {
                        if (_areas[k].OverlapPoint(new Vector2(x, y)))
                        {
                            coords[k].Add(new Vector3(j, 0f, i));
                            break;
                        }
                    }
                }
            }

            var _gates = (from c in children
                          where c.gameObject.CompareTag("Gate")
                          select c).ToArray();

            var gates = new List<Vector3>[_areas.Length];
            for (int i = 0; i < gates.Length; ++i)
            {
                gates[i] = new List<Vector3>();
            }

            for (int i = 0; i < _gates.Length; ++i)
            {
                for (int j = 0; j < _areas.Length; ++j)
                {
                    var pos = _gates[i].transform.position;
                    if (_areas[j].OverlapPoint(pos))
                    {
                        gates[j].Add(new Vector3(pos.x - 0.5f, 0f, pos.y - 0.5f + height));
                        break;
                    }
                }
            }

            idx2area = new Area[_areas.Length];
            name2area = new Dictionary<string, Area>();
            Area area;
            for (int i = 0; i < _areas.Length; ++i)
            {
                area = new Area(_areas[i], _areas[i].name, i, coords[i].ToArray(), gates[i].ToArray());
                idx2area[i] = area;
                name2area[area.Name] = area;
            }
        }

        // Adjust World Rotation & Position
        world.transform.Rotate(new Vector3(90f, 0f, 0f));
        world.transform.position = new Vector3(-0.5f, 0f, 44.5f);

        // Initialize Paths
        {
            paths = new Path[idx2area.Length, idx2area.Length];
            NavMeshPath path;
            float distance;
            NavMeshPath minPath;
            float minDistance;
            for (int i = 0; i < idx2area.Length; ++i)
            {
                for (int j = 0; j < idx2area.Length; ++j)
                {
                    if (i == j) { continue; }

                    minPath = null;
                    minDistance = float.MaxValue;

                    for (int k = 0; k < idx2area[i].Gates.Length; ++k)
                    {
                        for (int u = 0; u < idx2area[j].Gates.Length; ++u)
                        {
                            path = new NavMeshPath();
                            NavMesh.CalculatePath(idx2area[i].Gates[k], idx2area[j].Gates[u], NavMesh.AllAreas, path);
                            distance = Distance(path);
                            if (distance < minDistance)
                            {
                                minDistance = distance;
                                minPath = path;
                            }
                        }
                    }
                    paths[i, j] = new Path(idx2area[i], idx2area[j], minPath);
                }
            }
        }

        // Debug
        if (false)
        {
            for (int i = 0; i < idx2area.Length; ++i)
            {
                Debug.Log(string.Format("{0} : Gates = {1}, Coords = {2}", idx2area[i].Name, idx2area[i].Gates.Length, idx2area[i].Coords.Length));
            }

            for (int i = 0; i < idx2area.Length; ++i)
            {
                for (int j = 0; j < idx2area.Length; ++j)
                {
                    if (i == j) { continue; }

                    var l = Instantiate(line);
                    var lr = l.GetComponent<LineRenderer>();
                    var p = paths[i, j];
                    lr.positionCount = p.NavMeshPath.corners.Length;
                    lr.SetPositions(p.NavMeshPath.corners);
                }
            }

            for (int i = 0; i < idx2area.Length; ++i)
            {
                var l = Instantiate(line);
                var lr = l.GetComponent<LineRenderer>();
                lr.positionCount = idx2area[i].Coords.Length;
                lr.SetPositions(idx2area[i].Coords);
            }
        }
    }

    float Distance(NavMeshPath path)
    {
        float distance = 0f;
        for (int i = 0; i < path.corners.Length - 1; ++i)
        {
            distance += Vector3.Distance(path.corners[i], path.corners[i + 1]);
        }
        return distance;
    }

    public Area GetArea(string name)
    {
        return name2area[name];
    }

    public Area GetArea(int index)
    {
        return idx2area[index];
    }

    public Path GetPath(string srcAreaName, string dstAreaName)
    {
        return paths[name2area[srcAreaName].Index, name2area[dstAreaName].Index];
    }

    public Path GetPath(int srcAreaIdx, int dstAreaIdx)
    {
        return paths[srcAreaIdx, dstAreaIdx];
    }
}

namespace GameCore
{
    //[System.Serializable]
    public class Area
    {
        Collider2D collider;

        string name;

        //[SerializeField]
        int index;
        Vector3[] coords;
        Vector3[] gates;

        public Collider2D Collider
        {
            get { return collider; }
        }

        public string Name
        {
            get { return name; }
        }

        public int Index
        {
            get { return index; }
        }

        public Vector3[] Coords
        {
            get { return coords; }
        }

        public Vector3[] Gates
        {
            get { return gates; }
        }

        public Area(Collider2D collider, string name, int index, Vector3[] coords, Vector3[] gates)
        {
            this.collider = collider;
            this.name = name;
            this.index = index;
            this.coords = coords;
            this.gates = gates;
        }

        public Vector3 GetRandomCoord()
        {
            return coords[Random.Range(0, coords.Length)];
        }
    }

    public class Path
    {
        Area sourceArea;
        Area destinationArea;
        NavMeshPath navMeshPath;

        public Area SourceArea
        {
            get { return SourceArea; }
        }

        public Area DestinationArea
        {
            get { return destinationArea; }
        }

        public Vector3 Source
        {
            get { return navMeshPath.corners[0]; }
        }

        public Vector3 Destination
        {
            get { return navMeshPath.corners[navMeshPath.corners.Length - 1]; }
        }

        public NavMeshPath NavMeshPath
        {
            get { return navMeshPath; }
        }

        public Path(Area sourceArea, Area destinationArea, NavMeshPath navMeshPath)
        {
            this.sourceArea = sourceArea;
            this.destinationArea = destinationArea;
            this.navMeshPath = navMeshPath;
        }
    }
}