﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectOneGroup : MonoBehaviour {

    SelectOne selected;

    [SerializeField]
    SelectOne[] members;

    public SelectOne Selected
    {
        get { return selected; }
    }

    public void Start()
    {
        selected = null;
        if (members != null)
        {
            foreach (SelectOne so in members)
            {
                Register(so);
            }
        }
    }

    public void Register(SelectOne one)
    {
        one.onSelectChanged += OnSelectedChanged;
    }

    public void Unregister(SelectOne one)
    {
        one.onSelectChanged -= OnSelectedChanged;
    }

    private void OnSelectedChanged(object sender, System.EventArgs e)
    {
        var so = (SelectOne)sender;

        if (so.Selected)
        {
            if (selected != null)
            {
                selected.Unselect();
            }
            selected = so;
        }
        else
        {
            selected = null;
        }
    }
}
