﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    bool mouseDown;
    Vector3 prevMousePos;
    Vector3 initMousePos;
    //Vector3 initCameraPos;
    Camera _camera;

	// Use this for initialization
	void Start () {
        mouseDown = false;
        _camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        

        if (Input.GetMouseButtonDown(0))
        {
            prevMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //initMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //initCameraPos = transform.position;
            mouseDown = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            mouseDown = false;
        }
		else if (mouseDown)
        {
            Vector3 currMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //transform.position = initCameraPos + (initMousePos - currMousePos);
            transform.Translate(prevMousePos - currMousePos, Space.World);
        }

        _camera.orthographicSize = Mathf.Max(_camera.orthographicSize - Input.mouseScrollDelta.y, 1);
	}
}
