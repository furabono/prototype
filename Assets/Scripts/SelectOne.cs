﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SelectOne : MonoBehaviour {

    bool selected = false;
    //SelectOneGroup group;

    public event System.EventHandler onSelectChanged;

    public bool Selected
    {
        get { return selected; }
    }

    public void Select()
    {
        if (!selected)
        {
            selected = true;
            onSelectChanged(this, System.EventArgs.Empty);
        }
    }

    public void Unselect()
    {
        if (selected)
        {
            selected = false;
            onSelectChanged(this, System.EventArgs.Empty);
        }
    }
}
