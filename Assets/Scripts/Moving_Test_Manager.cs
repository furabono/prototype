﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using GameCore.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Linq.Expressions;

public class Moving_Test_Manager : MonoBehaviour {

    [SerializeField]
    GameObject _menuPanel;

    [SerializeField]
    GameObject citizenPrefab;

    [SerializeField]
    Transform citizenHolder;

    [SerializeField] Tilemap _tilemap;
    [SerializeField] Camera _mainCamera;

    int numCitizen;

    [SerializeField]
    GameObject world;

    BaseLib.Classes.WorldBase _worldbase;

    Type citizen_t;
    Type world_t;
    public BaseLib.Classes.CitizenBase[] _citizens;
    public BaseLib.Classes.ActionMetaBase[] _actionMetas;

    void Start () {
        _tilemap.Load("map0");
        _tilemap.transform.position = new Vector3(_tilemap.Width / 2 - 0.5f, _tilemap.Height / 2 - 0.5f, 0);
        _mainCamera.transform.position = new Vector3(_tilemap.Width / 2 - 0.5f, _tilemap.Height / 2 - 0.5f, _mainCamera.transform.position.z);

        citizen_t = DataManager.Instance.CitizenType;
        world_t = DataManager.Instance.WorldType;
        _actionMetas = DataManager.Instance.ActionMetas;

        numCitizen = 1;
        if (File.Exists(Application.streamingAssetsPath + "/population.txt"))
        {
            var str = File.ReadAllText(Application.streamingAssetsPath + "/population.txt");
            int.TryParse(str, out numCitizen);
        }

        Dictionary<string, int> nameToIndex;
        int[][] values;
        bool dataLoaded = false;
        if (dataLoaded = LoadCitizenData(out nameToIndex, out values))
        {
            numCitizen = values.Length;
        }

        _worldbase = world.AddComponent(world_t) as BaseLib.Classes.WorldBase;
        GenerateCitizen();

        if (dataLoaded)
        {
            for (int i = 0; i < _citizens.Length; ++i)
            {
                _citizens[i].InitCharacteristics(nameToIndex, values[i]);
            }
        }
    }
	
    void GenerateCitizen()
    {
        _citizens = new BaseLib.Classes.CitizenBase[numCitizen];

        GameObject obj;
        for (int i = 0; i < numCitizen; ++i)
        {
            obj = Instantiate(citizenPrefab);
            _citizens[i] = obj.AddComponent(citizen_t) as BaseLib.Classes.CitizenBase;
            obj.transform.SetParent(citizenHolder);
            obj.transform.Find("Sprite").GetComponent<SpriteRenderer>().sortingOrder = _tilemap.CitizenOrder;
        }
    }

    bool LoadCitizenData(out Dictionary<string, int> nameToIndex, out int[][] values)
    {
        string path = Application.streamingAssetsPath + "/citizen.json";
        if (!File.Exists(path))
        {
            nameToIndex = null;
            values = null;
            return false;
        }

        JObject jobject;

        using (StreamReader sr = File.OpenText(path))
        using (JsonTextReader reader = new JsonTextReader(sr))
        {
            jobject = JObject.Load(reader);
        }

        var characteristics = (JArray)jobject["Parameter"];
        var _nameToIndex = new Dictionary<string, int>();
        for (int i = 0; i < characteristics.Count; ++i)
        {
            _nameToIndex.Add(characteristics[i]["Name"].Value<string>(), i);
        }
        nameToIndex = _nameToIndex;

        var _citizens = (JArray)jobject["Citizen"];
        var _values = new int[_citizens.Count][];
        for (int i = 0; i < _citizens.Count; ++i)
        {
            _values[i] = (from v in _citizens[i] select v.Value<int>()).ToArray();
        }
        values = _values;

        return true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_pause) { Resume(); }
            else { Pause(); }
        }

        if (_pause) { return; }

        _worldbase.ManualUpdate();
        for (int i = 0; i < _citizens.Length; ++i)
        {
            _citizens[i].ManualUpdate();
        }

        for (int i = 0; i < _actionMetas.Length; ++i)
        {
            _actionMetas[i].ManualUpdate();
        }

        _worldbase.NextTick();
    }

    bool _pause;
    void Pause()
    {
        _menuPanel.transform.localScale = new Vector3(1, 1, 1);
        _pause = true;
    }

    public void Resume()
    {
        _menuPanel.transform.localScale = new Vector3(0, 0, 1);
        _pause = false;
    }

    public void Exit()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
    }
}

class MinHeap<TPriority, TValue>
    where TPriority : IComparable<TPriority>
{
    List<Tuple<TPriority, TValue>> list;

    public bool Empty
    {
        get { return list.Count == 0; }
    }

    private void ShiftUp(int pos)
    {
        int p;
        while (pos > 0)
        {
            p = (pos - 1) / 2;
            if (list[pos].Item1.CompareTo(list[p].Item1) >= 0) { break; }

            Tuple<TPriority, TValue> t = list[pos];
            list[pos] = list[p];
            list[p] = t;
            pos = p;
        }
    }

    private void ShiftDown(int pos)
    {
        int count = list.Count;
        int lastParent = (list.Count - 1 / 2);

        int c;
        int cm;
        while (pos < lastParent)
        {
            c = pos * 2 + 1;
            cm = c;
            if (c + 1 < count && list[c].Item1.CompareTo(list[c + 1].Item1) > 0) { cm = c + 1; }
            if (list[pos].Item1.CompareTo(list[cm].Item1) <= 0) { break; }

            Tuple<TPriority, TValue> t = list[pos];
            list[pos] = list[cm];
            list[cm] = t;
            pos = cm;
        }
    }

    public void Push(TPriority priority, TValue value)
    {
        Tuple<TPriority, TValue> node = Tuple.Create(priority, value);
        list.Add(node);
        ShiftUp(list.Count - 1);
    }

    public TValue Peek()
    {
        return list[0].Item2;
    }

    public TValue Pop()
    {
        TValue peek = list[0].Item2;
        list[0] = list[list.Count - 1];
        list.RemoveAt(list.Count - 1);
        ShiftDown(0);
        return peek;
    }
}