﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class CivilLoadScene : MonoBehaviour {

    [SerializeField]
    GameObject civilUI;

    [SerializeField]
    GameObject policyUI;

    [SerializeField]
    GameObject LoadingUI;

    [SerializeField]
    CivilManager civilManager;

    [SerializeField]
    PolicyManager policyManager;

    [SerializeField]
    Report report;

	// Use this for initialization
	void Start () {
        civilUI.SetActive(false);
        policyUI.SetActive(false);
        LoadingUI.SetActive(false);

        StartCoroutine(Init());
	}

    IEnumerator Init()
    {
        yield return StartCoroutine(LoadPolicy());

        civilManager.Init();
        civilUI.SetActive(true);
    }

    IEnumerator LoadPolicy()
    {
        LoadingUI.SetActive(true);

        policyManager.Init();

        yield return StartCoroutine(policyManager.LoadPolicyCoroutine());

        LoadingUI.SetActive(false);
    }

    public void OnCivilSelectButtonClick()
    {
        FileInfo file = civilManager.GetSelectedFile();

        if (file != null)
        {
            StartCoroutine(LoadCivil(file));
        }
    }

    IEnumerator LoadCivil(FileInfo file)
    {
        civilUI.SetActive(false);
        LoadingUI.SetActive(true);

        var task = Task.Run(() => civilManager.LoadCivil(file));

        while (!task.IsCompleted) yield return null;

        LoadingUI.SetActive(false);
        policyUI.SetActive(true);
    }
}
