﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SelectOne))]
public class SelectButton : MonoBehaviour {

    [SerializeField]
    Color selectedColor;

    [SerializeField]
    Color unselectedColor;

    Image image;
    SelectOne so;

	void Start ()
    {
        image = GetComponent<Image>();
        image.color = unselectedColor;

        so = GetComponent<SelectOne>();
        so.onSelectChanged += So_onSelectChanged;
	}

    private void So_onSelectChanged(object sender, System.EventArgs e)
    {
        var so = (SelectOne)sender;

        if (so.Selected)
        {
            image.color = selectedColor;
        }
        else
        {
            image.color = unselectedColor;
        }
    }

    public void OnClick()
    {
        so.Select();
    }
}
