﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugUIManager : MonoBehaviour {

    [SerializeField]
    new Camera camera;

    [SerializeField]
    Moving_Test_Manager mtm;

    [SerializeField]
    ActionStatusDisplay actionStatusDisplay;

    [SerializeField]
    UnityEngine.UI.Text actionStatistics;

	void Start () {
        actionStatusDisplay.gameObject.SetActive(false);
	}
	
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Citizen hitCitizen = hit.collider.GetComponent<Citizen>();
                if (hitCitizen)
                {
                    ShowActionStatusDisplay(hitCitizen);
                }
            }
        }

        var actionPopulation = new Dictionary<string, int>();
        /*for (int i = 0; i < mtm.citizens.Length; ++i)
        {
            if (mtm.citizens[i].CurrentObjective == null) { continue; }
            if (!actionPopulation.ContainsKey(mtm.citizens[i].CurrentObjective.Name))
            {
                actionPopulation[mtm.citizens[i].CurrentObjective.Name] = 1;
            }
            else
            {
                ++actionPopulation[mtm.citizens[i].CurrentObjective.Name];
            }
        }*/

        var actions = new string[actionPopulation.Count];
        var populations = new int[actionPopulation.Count];
        {
            int i = 0;
            foreach (var item in actionPopulation)
            {
                actions[i] = item.Key;
                populations[i] = item.Value;
                ++i;
            }
        }

        {
            Queue<int> quickQue = new Queue<int>();
            quickQue.Enqueue(0);
            quickQue.Enqueue(actions.Length - 1);

            int left, right;
            int p;
            int i, j;
            while (quickQue.Count > 1)
            {
                left = quickQue.Dequeue();
                right = quickQue.Dequeue();

                if (right - left < 1) { break; }

                i = left;
                j = right;
                p = populations[(left + right) / 2];
                do
                {
                    while (populations[i] > p) { ++i; }
                    while (populations[j] < p) { --j; }
                    if (i <= j)
                    {
                        Swap(populations, i, j);
                        Swap(actions, i, j);
                        ++i;
                        --j;
                    }
                } while (i < j);

                quickQue.Enqueue(left);
                quickQue.Enqueue(j);
                quickQue.Enqueue(i);
                quickQue.Enqueue(right);
            }
        }
        
        string text = "";
        for (int i = 0; i < actions.Length; ++i)
        {
            text += string.Format("{0} : {1}\n", actions[i], populations[i]);
        }
        actionStatistics.text = text;
	}

    void Swap<T>(T[] array, int index1, int index2)
    {
        T temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    void ShowActionStatusDisplay(Citizen citizen)
    {
        actionStatusDisplay.gameObject.SetActive(true);
        actionStatusDisplay.SetTarget(citizen);
    }
}
