﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {

    static World instance;

    int tick;
    int time;

    public static World Instance
    {
        get { return instance; }
    }

    public static int Tick
    {
        get { return instance.tick; }
    }

    public static int Time
    {
        get { return instance.time; }
    }

    private void Awake()
    {
        if (instance == null)
        {
            tick = 0;
            time = 0;
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }
	
	void Update () {
        ++tick;
        time = (tick % 1440);
	}
}
