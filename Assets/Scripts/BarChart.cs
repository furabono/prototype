﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarChart : MonoBehaviour {

    [SerializeField]
    GameObject bar;

    [SerializeField]
    GameObject label;

    Transform barHolder;
    Transform labelHolder;

    List<GameObject> bars;
    List<GameObject> labels;
    bool initialized = false;

    public int PaddingLeft
    {
        get { return barHolder.GetComponent<HorizontalLayoutGroup>().padding.left - 5; }
        set
        {
            barHolder.GetComponent<HorizontalLayoutGroup>().padding.left = value + 5;
            labelHolder.GetComponent<HorizontalLayoutGroup>().padding.left = value;
        }
    }

    public int PaddingRight
    {
        get { return barHolder.GetComponent<HorizontalLayoutGroup>().padding.right; }
        set
        {
            barHolder.GetComponent<HorizontalLayoutGroup>().padding.right = value;
            labelHolder.GetComponent<HorizontalLayoutGroup>().padding.right = value;
        }
    }

    public float Spacing
    {
        get { return barHolder.GetComponent<HorizontalLayoutGroup>().spacing; }
        set
        {
            barHolder.GetComponent<HorizontalLayoutGroup>().spacing = value;
            labelHolder.GetComponent<HorizontalLayoutGroup>().spacing = value;
        }
    }

    public int Count
    {
        get { return bars.Count; }
    }

    void Awake ()
    {
        Init();
    }

    public void Init()
    {
        if (initialized) { return; }

        barHolder = transform.Find("Bar Holder");
        labelHolder = transform.Find("Label Holder");

        bars = new List<GameObject>();
        labels = new List<GameObject>();

        initialized = true;
        Debug.Log("Bar Chart Initialized");
    }

    public void AddBar(float height, string barText, string labelText)
    {
        int index = bars.Count;

        GameObject bar = Instantiate(this.bar);
        bar.transform.SetParent(barHolder);
        bar.transform.localScale = Vector3.one;
        bars.Add(bar);
        SetBarHeight(index, height);
        SetBarText(index, barText);

        GameObject label = Instantiate(this.label);
        label.transform.SetParent(labelHolder);
        label.transform.localScale = Vector3.one;
        labels.Add(label);
        SetLabelText(index, labelText);
    }

    public void SetBarHeight(int index, float height)
    {
        GameObject bar = bars[index];

        height = height * barHolder.GetComponent<RectTransform>().rect.height;
        bar.GetComponent<RectTransform>().sizeDelta = new Vector2(0, height);
    }

    public void SetBarText(int index, string barText)
    {
        GameObject bar = bars[index];
        bar.transform.Find("Text").GetComponent<Text>().text = barText;
    }

    public void SetLabelText(int index, string labelText)
    {
        GameObject label = labels[index];
        label.transform.Find("Text").GetComponent<Text>().text = labelText;
    }

    public void RemoveBar(int index)
    {
        Destroy(bars[index]);
        bars.RemoveAt(index);

        Destroy(labels[index]);
        labels.RemoveAt(index);
    }
}
