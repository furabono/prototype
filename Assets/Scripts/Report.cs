﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Report : MonoBehaviour {

    [SerializeField]
    BarChart barChart;

    public void OnCloseButtonClick()
    {
        gameObject.SetActive(false);
    }

    public void ShowReport(ReportData data)
    {
        barChart.Init();

        int n;
        if (barChart.Count < data.Count)
        {
            for (int i = barChart.Count; i < data.Count; ++i)
            {
                barChart.AddBar(data.Heights[i], data.Volumes[i].ToString(), data.LabelTexts[i]);
            }
            n = barChart.Count;
        }
        else
        {
            for (int i = data.Count; i < barChart.Count; ++i)
            {
                barChart.RemoveBar(i);
            }
            n = data.Count;
        }

        for (int i = 0; i < n; ++i)
        {
            barChart.SetBarHeight(i, data.Heights[i]);
            barChart.SetBarText(i, data.Volumes[i].ToString());
            barChart.SetLabelText(i, data.LabelTexts[i]);
        }

        gameObject.SetActive(true);
    }
}

public class ReportData
{
    int[] volumes;
    float[] heights;
    string[] labelTexts;

    public int[] Volumes
    {
        get { return volumes; }
    }

    public float[] Heights
    {
        get { return heights; }
    }

    public string[] LabelTexts
    {
        get { return labelTexts; }
    }

    public int Count
    {
        get { return volumes.Length; }
    }

    private ReportData(int[] volumes)
    {
        this.volumes = volumes;
        heights = new float[volumes.Length];
        float max = Mathf.Max(volumes);
        for (int i = 0; i < volumes.Length; ++i)
        {
            heights[i] = volumes[i] / max;
        }
    }

    public ReportData(int[] volumes, float[] values) : this(volumes)
    {
        if (volumes.Length != values.Length) { throw new ArgumentException("volumes, values must have same length"); }

        labelTexts = new string[values.Length];
        for (int i = 0; i < values.Length; ++i)
        {
            labelTexts[i] = values[i].ToString();
        }
    }

    public ReportData(int[] volumes, Tuple<float,float>[] classes) : this(volumes)
    {
        if (volumes.Length != classes.Length) { throw new ArgumentException("volumes, values must have same length"); }

        labelTexts = new string[classes.Length];
        for (int i = 0; i < classes.Length; ++i)
        {
            labelTexts[i] = string.Format("[{0},\n{1})", classes[i].Item1, classes[i].Item2);
        }
    }
}