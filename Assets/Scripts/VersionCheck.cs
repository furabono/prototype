﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionCheck : MonoBehaviour {

    public string version;

	void Start () {
        var v = new System.Version();
        version = v.ToString();

        var txt = GetComponent<UnityEngine.UI.Text>();
        txt.text = version;
	}
}
